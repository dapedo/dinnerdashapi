Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :users, param: :_name
  post '/auth/login', to: 'authentication#login'
  get '/*', to: 'application#not_found'

  get '/mealcategories', to: 'meal_categories#index'
  get '/mealcategories/:id', to: 'meal_categories#show'
  post '/mealcategories', to: 'meal_categories#create'
  put '/mealcategories/:id', to: 'meal_categories#update'
  patch '/mealcategories/:id', to: 'meal_categories#update'
  delete '/mealcategories/:id', to: 'meal_categories#delete'
  
  get '/meals', to: 'meals#index'
  get '/meals/:id', to: 'meals#show'
  post '/meals', to: 'meals#create'
  put '/meals/:id', to: 'meals#update'
  patch '/meals/:id', to: 'meals#update'
  delete '/meals/:id', to: 'meals#delete'

  get '/situations', to: 'situations#index'
  get '/situations/:id', to: 'situations#show'
  post '/situations', to: 'situations#create'
  put '/situations/:id', to: 'situations#update'
  patch '/situations/:id', to: 'situations#update'
  delete '/situations/:id', to: 'situations#delete'

  get '/orders', to: 'orders#index'
  get '/orders/:id', to: 'orders#show'
  post '/orders', to: 'orders#create'
  put '/orders/:id', to: 'orders#update'
  patch '/orders/:id', to: 'orders#update'
  delete '/orders/:id', to: 'orders#delete'

  get '/ordermeals', to: 'order_meals#index'
  get '/ordermeals/:id', to: 'order_meals#show'
  post '/ordermeals', to: 'order_meals#create'
  put '/ordermeals/:id', to: 'order_meals#update'
  patch '/ordermeals/:id', to: 'order_meals#update'
  delete '/ordermeals/:id', to: 'order_meals#delete'

end
