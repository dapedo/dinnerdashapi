class FixDataType < ActiveRecord::Migration[6.0]
  def change
    change_column :meals, :available, 'boolean USING CAST(available AS boolean)'
  end
  
end
