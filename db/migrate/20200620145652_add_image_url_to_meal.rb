class AddImageUrlToMeal < ActiveRecord::Migration[6.0]
  def change
    add_column :meals, :image_url, :string
  end
end
