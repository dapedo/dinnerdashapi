# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user_ = User.create([
    {name: "Leo", email: "leo@gmail.com", password: "000000",password_confirmation: "000000", admin: true},
    {name: "Dapedo", email: "dapedo@gmail.com", password: "000000",password_confirmation: "000000", admin: false},
    {name: "Polly", email: "polly@perfeita.com", password: "000000",password_confirmation: "000000", admin: true}
])

situations = Situation.create([
    {description: 'Pendente'}, 
    {description: 'Entregue'}, 
    {description: 'Cancelado'}
])

mealCategories = MealCategory.create([
    {name: 'Massas'}, 
    {name: 'Carnes'},
    {name: 'Legumes'}
])

meals = Meal.create([
    {name: 'Lasagna', description: 'O melhor prato do mundo', price: 30, available: true, meal_category_id: 1}, 
    {name: 'Almoldêgas Suecas', description: 'O Sr. Leoncio gosta', price: 5, available: true, meal_category_id:  2}, 
    {name: 'Brócolis', description: 'Comam brócolis crianças', price: 2, available: true, meal_category_id: 3} 
])

orders = Order.create([
    {situation_id: 1, user_id: 1}, 
    {situation_id: 2, user_id: 2}, 
    {situation_id: 3, user_id: 3} 
])

orderMeals = OrderMeal.create([
    {order_id: 1, meal_id: 1},
    {quantity: 2, order_id: 2, meal_id: 2},
    {quantity: 3, order_id: 3, meal_id: 3}
])

meals.each do |meal|
    meal.image.attach(io: File.open(Rails.root.join('app/assets/images/todoroki.jpg')), filename: 'todoroki.jpg', content_type: 'image/jpg')
end    