class MealCategoriesController < ApplicationController
  before_action :set_meal_category, only: [:show, :update, :delete]
  before_action :auth_admin_request, only: [:create, :update, :delete]

  #GET '/mealcategories'
  def index
    @q= MealCategory.ransack(params[:q])
    @mealcat = @q.result(distinct: true)
    render json: @mealcat, status: 200
  end  

  #GET '/mealcategories/:id'
  def show 
    render json: @mealcat, status: 200
  end  

  #POST '/mealcategories'
  def create 
    @mealcat = MealCategory.new(meal_category_params)
      if @mealcat.save 
        render json: @mealcat, status: :created
      else
        render @mealcat.errors, status: :unprocessable_entity
      end   
  end 

  #PUT/PATCH '/mealcategories/:id'
  def update
    if @mealcat.update(meal_category_params)
      render json: @mealcat, status: 200   
    else
      render json: @mealcat.errors, status: :unprocessable_entity
    end    
  end  

  #DELETE '/mealcategories/:id'
  def delete 
    @mealcat.destroy
  end  

  private

  def set_meal_category
    @mealcat = MealCategory.find(params[:id])
  end

  def meal_category_params
    params.require(:meal_category).permit(:name)
  end
end
