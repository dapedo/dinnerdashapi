class OrderMealsController < ApplicationController
    before_action :set_order_meal, only: [:show, :update, :delete]
    before_action :auth_request
    before_action :auth_user, only: [:update, :delete]

    #GET '/ordermeals'
    def index
        @q= OrderMeal.ransack(params[:q])
        @ordermeals = @q.result(distinct: true)
        render json: @ordermeals, status: 200
    end  

    #GET '/ordermeals/:id'
    def show 
        render json: @ordermeal, status: 200
    end  

    #POST '/ordermeals'
    def create 
        @ordermeal = OrderMeal.new(order_meal_params)
        if Order.where(user_id: @current_user.id).ids.include? (@ordermeal.order_id)
            if @ordermeal.save 
            render json: @ordermeal, status: :created
            else
            render @ordermeal.errors, status: :unprocessable_entity
            end   
        else 
            render json: {error: 'order_id must be yours'}, status: :unauthorized
        end
    end 

    #PUT/PATCH '/ordermeals/:id'
    def update
        if @ordermeal.update(order_meal_params)
        render json: @ordermeal, status: 200   
        else
        render json: @ordermeal.errors, status: :unprocessable_entity
        end    
    end  

    #DELETE '/ordermeals/:id'
    def delete  
        @ordermeal.destroy
    end  

    private

    def set_order_meal
        @ordermeal = OrderMeal.find(params[:id])
    end

    def order_meal_params
        params.require(:order_meal).permit(:quantity, :order_id, :meal_id)
    end

    def auth_user
        unless Order.where(user_id: @current_user.id).ids.include? (@ordermeal.order_id)
          render json: {error: 'order_id must be yours'}, status: :unauthorized
      end
    end
end
