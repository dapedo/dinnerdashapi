class MealsController < ApplicationController
  before_action :set_meal, only: [:show, :update, :delete]
  before_action :auth_admin_request, only: [:create, :update, :delete]
  after_action :set_image_url, only: :create

  #GET '/meals'
  def index
    @q = Meal.ransack(params[:q])
    @meal = @q.result(distinct: true)
    render json: @meal, status: 200
  end  

  #GET '/meals/:id'
  def show 
    render json: @meal, status: 200
  end  

  #POST '/meals'
  def create 
    @meal = Meal.new(meal_params)
    if @meal.save 
      render json: @meal, status: :created
    else
      render @meal.errors, status: :unprocessable_entity
    end     
  end 

  #PUT/PATCH '/meals/:id'
  def update
    if @meal.update(meal_params)
      render json: @meal, status: 200   
    else
      render json: @meal.errors, status: :unprocessable_entity
    end    
  end  

  #DELETE '/meals/:id'
  def delete 
    @meal.destroy
  end  

  private

  def set_meal
    @meal = Meal.find(params[:id])
  end

  def meal_params
      params.permit(:name, :price, :description, :available, :meal_category_id, :image)
  end

  def set_image_url
    if @meal.image.attached?
      @url = url_for(@meal.image)
      @meal.image_url = @url
      @meal.save
    end
  end  
end
