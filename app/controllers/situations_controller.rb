class SituationsController < ApplicationController
    before_action :set_situation, only: [:show, :update, :delete]
    before_action :auth_admin_request

    #GET '/situations'
    def index
        @q= Situation.ransack(params[:q])
        @situations = @q.result(distinct: true)       
        render json: @situations, status: 200
    end  

    #GET '/situations/:id'
    def show 
        render json: @situation, status: 200
    end  

    #POST '/situations'
    def create 
        @situation = Situation.new(situation_params)
        if @situation.save 
        render json: @situation, status: :created
        else
        render @situation.errors, status: :unprocessable_entity
        end     
    end 

    #PUT/PATCH '/situations/:id'
    def update
        if @situation.update(situation_params)
        render json: @situation, status: 200   
        else
        render json: @situation.errors, status: :unprocessable_entity
        end    
    end  

    #DELETE '/situations/:id'
    def delete 
        @situation.destroy
    end  

    private 
    
    def set_situation
        @situation = Situation.find(params[:id])
    end

    def situation_params
        params.require(:situation).permit(:description)
    end
end

