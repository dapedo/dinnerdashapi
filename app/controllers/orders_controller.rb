class OrdersController < ApplicationController
    before_action :set_order, only: [:show, :update, :delete]
    before_action :auth_request
    before_action :auth_user, only: [:update, :delete]

    #GET '/orders'
    def index
        if @current_user.is_admin?
            @q= Order.ransack(params[:q])
            @orders = @q.result(distinct: true)       
            render json: @orders, status: 200
        else
            @q = Order.where(user_id: @current_user.id).ransack(params[:q])
            @user_orders = @q.result(distict: true)
            render json: @user_orders, status: 200
        end    
    end  

    #GET '/orders/:id'
    def show 
        if @current_user.is_admin?
            render json: @order, status: 200
        else
            if Order.where(user_id: @current_user.id).ids.include?(params[:id].to_i)
                get_meals
                render json: {order: @order, meals: @meals}, status: 200
            else
                render json: { error: 'Wrong id!'}, status: :unprocessable_entity
            end
        end
    end

    #POST '/orders'
    def create 
        @order = Order.new(order_params)
        if @current_user.id == @order.user_id
            if @order.save 
            render json: @order, status: :created
            else
            render @order.errors, status: :unprocessable_entity
            end 
        else
            render json: {error: 'user_id must be yours'}, status: :unauthorized
        end
        
    end 

    #PUT/PATCH '/orders/:id'
    def update
        if @order.update(order_params)
        render json: @order, status: 200   
        else
        render json: @order.errors, status: :unprocessable_entity
        end    
    end  

    #DELETE '/orders/:id'
    def delete 
        @order.destroy
    end  

    private

    def set_order
        @order = Order.find(params[:id])
    end

    def order_params
        params.require(:order).permit(:situation_id, :user_id)
    end

    def get_meals
        @meals = Array.new
        @order.order_meals.each do |order_meal|
            @meals.append(
                {name: order_meal.meal.name, 
                price: order_meal.meal.price,
                quantidade: order_meal.quantity}
            )
        end
    end

    def auth_user
        unless @current_user.id == @order.id
          render json: {error: 'user_id must be yours'}, status: :unauthorized
      end
    end
end
