class MealCategory < ApplicationRecord
    validates :name, presence: true, uniqueness: true, length: { maximum: 45 }

    has_many :meals
end
