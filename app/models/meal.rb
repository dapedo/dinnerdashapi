class Meal < ApplicationRecord
    validates :name, presence: true, length: { maximum: 45 }
    validates :description, presence: true, length: { maximum: 45 }
    validates :price, presence: true, numericality: { greater_or_equal_to: 0 }
    validates :available, presence: true, length: { maximum: 45 }
    validates :meal_category_id, presence: true, numericality: { only_integer: true, greater_than: 0 }

    belongs_to :meal_category
    has_many :order_meals
    has_many :orders, through: :order_meals

    has_one_attached :image
end
