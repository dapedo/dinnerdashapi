class User < ApplicationRecord
    has_secure_password

    validates :name, presence: true
    validates :email, presence: true, uniqueness: true
    validates :password, length: { minimum: 6 },
    if: -> {new_record? || !password.nil?}

    has_many :orders, dependent: :destroy
    
    def is_admin?
        self.admin
    end
end
