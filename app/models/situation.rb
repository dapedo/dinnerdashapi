class Situation < ApplicationRecord
    validates :description, uniqueness: true, presence: true, length: {maximum: 45}

    has_many :orders
end
