class Order < ApplicationRecord
    before_create :set_price_to_0

    validates :situation_id, presence: true, numericality: { only_integer: true, greater_than: 0 }
    validates :user_id, presence: true, numericality: { only_integer: true, greater_than: 0 }

    belongs_to :situation
    belongs_to :user
    has_many  :order_meals, dependent: :destroy
    has_many :meals, through: :order_meals  

    private
        
    def set_price_to_0
        self.price = 0
    end       
end
