class OrderMeal < ApplicationRecord
    validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
    validates :order_id, presence: true, numericality: { only_integer: true, greater_than: 0 }
    validates :meal_id, presence: true, numericality: { only_integer: true, greater_than: 0 }

    before_validation :set_default_quantity_if_not_specified
    before_validation :set_price
    before_destroy :remove_price

    belongs_to :order
    belongs_to :meal

    private

    def set_default_quantity_if_not_specified
        self.quantity = 1 if self.quantity.nil?
    end

    def set_price 
        mealprice = self.quantity * self.meal.price
        self.order.price += mealprice
        self.order.save
    end    

    def remove_price
        mealprice = self.quantity * self.meal.price
        self.order.price -= mealprice
        self.order.save
    end    
end